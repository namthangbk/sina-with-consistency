﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ThongKe.Models
{
    public partial class VersionDatum
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public int Ver { get; set; }
    }
}
