﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ThongKe.Models
{
    public partial class Temp
    {
        public int Id { get; set; }
        public double Num { get; set; }
        public DateTime TimeRun { get; set; }
    }
}
