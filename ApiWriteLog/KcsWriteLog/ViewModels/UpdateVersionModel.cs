﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KcsWriteLog.ViewModels
{
    public class UpdateVersionModel
    {
        public string Ip { get; set; }
        public int Version { get; set; }
    }
}
