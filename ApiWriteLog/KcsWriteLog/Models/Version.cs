﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KcsWriteLog.Models
{
    public partial class Version
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public int Version1 { get; set; }
    }
}
