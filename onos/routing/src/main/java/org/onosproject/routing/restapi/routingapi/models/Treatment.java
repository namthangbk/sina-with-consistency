/**
 * models.
 */
package org.onosproject.routing.restapi.routingapi.models;

import java.util.List;

public class Treatment {
    public List<Instruction> instructions;
}
