/**
 * Instruction.
 */
package org.onosproject.routing.restapi.routingapi.models;

public class Instruction {
    public String type;
    public String port;
}
