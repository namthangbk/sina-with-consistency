/**
 * Selector.
 */
package org.onosproject.routing.restapi.routingapi.models;

import java.util.List;

public class Selector {
    public List<Criteria> criteria;
}
