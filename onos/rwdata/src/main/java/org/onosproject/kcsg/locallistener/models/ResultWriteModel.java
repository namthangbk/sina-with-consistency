package org.onosproject.kcsg.locallistener.models;

public class ResultWriteModel {
    private int length;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
